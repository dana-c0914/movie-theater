angular.module('theater').config(function($stateProvider) {
    $stateProvider.state({
        name: 'cooming-soon',
        url: '/cooming-soon',
        template: '<cooming-soon-layout></cooming-soon-layout>'
    });
});