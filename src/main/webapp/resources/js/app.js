'use strict';

var theater = angular
    .module('theater', ['restangular', 'ui.router', 'ngResource', 'ngRoute', 'swaggerUi', 'http-auth-interceptor', 'ngAnimate', 'angular-spinkit']);

theater.config(function ($routeProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.when('', "/home");

    // $routeProvider.when("/home", {
    //     templateUrl: "partials/home.html",
    //     controller: 'HomeController',
    //     access: {
    //         loginRequired: true,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when('/', {
    //     redirectTo: '/home'
    // }).when('/users', {
    //     templateUrl: 'partials/users.html',
    //     controller: 'UsersController',
    //     access: {
    //         loginRequired: true,
    //         authorizedRoles: [USER_ROLES.admin]
    //     }
    // }).when('/apiDoc', {
    //     templateUrl: 'partials/apiDoc.html',
    //     controller: 'ApiDocController',
    //     access: {
    //         loginRequired: true,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when('/tokens', {
    //     templateUrl: 'partials/tokens.html',
    //     controller: 'TokensController',
    //     access: {
    //         loginRequired: true,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when('/login', {
    //     templateUrl: 'partials/login.html',
    //     controller: 'LoginController',
    //     access: {
    //         loginRequired: false,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when('/loading', {
    //     templateUrl: 'partials/loading.html',
    //     access: {
    //         loginRequired: false,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when("/logout", {
    //     template: " ",
    //     controller: "LogoutController",
    //     access: {
    //         loginRequired: false,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).when("/error/:code", {
    //     templateUrl: "partials/error.html",
    //     controller: "ErrorController",
    //     access: {
    //         loginRequired: false,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // }).otherwise({
    //     redirectTo: '/error/404',
    //     access: {
    //         loginRequired: false,
    //         authorizedRoles: [USER_ROLES.all]
    //     }
    // });
});