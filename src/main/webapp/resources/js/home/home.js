angular.module('theater').config(function($stateProvider) {
    $stateProvider.state({
        name: 'home',
        url: '/home',
        template: '<home-layout></home-layout>'
    });
});