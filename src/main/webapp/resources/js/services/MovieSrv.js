angular.module('theater').service('MovieSrv', function (MovieRestangular) {
    var service = this;
    service.movies = MovieRestangular.all('');

    service.getAllMovies = function () {
        return service.movies.getList();
    };
});