angular.module('theater').constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    user: 'user'
});