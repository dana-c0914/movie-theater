angular.module('theater').config(function($stateProvider) {
    $stateProvider.state({
        name: 'contact',
        url: '/contact',
        template: '<contact-layout></contact-layout>'
    });
});