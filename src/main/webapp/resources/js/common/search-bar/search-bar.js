'use strict';

angular.module('theater').component('searchBar', {
    templateUrl: 'resources/js/common/search-bar/search-bar.html',
    controller: function () {
        var $ctrl = this;

        $ctrl.searchText = '';
        $ctrl.search = function () {
            console.log("search by ", $ctrl.searchText);
        };
    }
});