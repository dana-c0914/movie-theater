angular.module('theater')
    .factory('MovieRestangular', function (Restangular) {
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl('/movies');
        });
    })