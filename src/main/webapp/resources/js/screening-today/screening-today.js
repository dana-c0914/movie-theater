'use strict';

angular.module('theater').component('screeningToday', {
    templateUrl: 'resources/js/screening-today/screening-today.html',
    controller: function (MovieSrv) {
        var $ctrl = this;
        MovieSrv.getAllMovies().then(function (response) {
            $ctrl.movies = response;
        });

        $ctrl.startCarousel = function () {
            $('.owl-carousel').owlCarousel({
                // loop:true,
                // margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })
        };

        // $timeout(function () {
        //     $('.owl-carousel').owlCarousel({
        //         loop:true,
        //         margin:10,
        //         nav:true,
        //         responsive:{
        //             0:{
        //                 items:1
        //             },
        //             600:{
        //                 items:3
        //             },
        //             1000:{
        //                 items:5
        //             }
        //         }
        //     })
        // });
    }
});