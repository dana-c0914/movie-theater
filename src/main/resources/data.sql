insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Steve', 'JOBS', 'steve', 'steve', 'steve.jobs@apple.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Bill', 'GATES', 'bill', 'bill', 'bill.gates@microsoft.com', '0033 1 23 45 67 89', 'fr', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Mark', 'ZUCKERBERG', 'mark', 'zuckerberg', 'mark.zuckerberg@facebook.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Tim', 'COOK', 'tim', 'cook', 'tim.cook@apple.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Larry', 'Page', 'larry', 'page', 'larry.page@gmail.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Sergey', 'Brin', 'sergey', 'brin', 'sergey.brin@gmail.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Larry', 'ELLISON', 'larry2', 'ellison', 'larry.ellison@oracle.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Jeff', 'BEZOS', 'jeff', 'bezos', 'jeff.bezos@amazon.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Paul', 'ALLEN', 'paul', 'allen', 'paul.allen@microsoft.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Steve', 'BALLMER', 'steve2', 'ballmer', 'steve.ballmer@microsoft.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Jack', 'DORSEY', 'jack', 'dorsey', 'jack.dorsey@twitter.com', '0033 1 23 45 67 89', 'en', true);
insert into users (first_name, family_name, login, password, e_mail, phone, language, enabled) values ('Matt', 'MULLENWEG', 'matt', 'mullenweg', 'matt.mullenweg@wordpress.com', '0033 1 23 45 67 89', 'en', true);

insert into authority (name) values ('admin');
insert into authority (name) values ('technical user');
insert into authority (name) values ('user');

insert into users_authority (id_user, id_authority) values (1, 1);
insert into users_authority (id_user, id_authority) values (1, 2);
insert into users_authority (id_user, id_authority) values (1, 3);
insert into users_authority (id_user, id_authority) values (2, 3);
insert into users_authority (id_user, id_authority) values (3, 3);

insert into Movie (title, description, release_year, length_minutes) values
  ('Frumoasa si Bestia','An adaptation of the Disney fairy tale about a monstrous-looking prince and a young woman who fall in love.', 2017, 129),
  ('Logan','In the near future, a weary Logan cares for an ailing Professor X somewhere on the Mexican border. However, Logan\'s attempts to hide from the world and his legacy are upended when a young mutant arrives, pursued by dark forces.', 2017, 137),
  ('Sing','In a city of humanoid animals, a hustling theater impresario\'s attempt to save his theater with a singing competition becomes grander than he anticipates even as its finalists\' find that their lives will never be the same.',2016,108),
  ('Moana','In Ancient Polynesia, when a terrible curse incurred by the Demigod Maui reaches an impetuous Chieftain\'s daughter\'s island, she answers the Ocean\'s call to seek out the Demigod to set things right.',2016,107),
  ('Kong: Skull Island','A team of scientists explore an uncharted island in the Pacific, venturing into the domain of the mighty Kong, and must fight to escape a primal Eden.',2017,118),
  ('The Fate of the Furious','When a mysterious woman seduces Dom into the world of crime and a betrayal of those closest to him, the crew face trials that will test them as never before.',2017,136),
  ('Passengers','A spacecraft traveling to a distant colony planet and transporting thousands of people has a malfunction in its sleep chambers. As a result, two passengers are awakened 90 years early.',2016,116),
  ('Fantastic Beasts and Where to Find Them','The adventures of writer Newt Scamander in New York\'s secret community of witches and wizards seventy years before Harry Potter reads his book in school.',2016,133);

insert into Category (title) values
  ('Action'),('Adventure'),('Animation'),('Biography'),('Comedy'),('Crime'),('Documentary'),('Drama'),('Family'),('Fantasy'),('Film-Noir'),('History'),('Horror'),('Music'),('Musical'),('Mystery'),('Romance'),('Sci-Fi'),('Sport'),('Thriller'),('War'),('Western');

insert into Movie_Category (movie_id, category_id) values
  (1,9),(1,10),(1,15),(2,1),(2,8),(2,18),(3,3),(3,5),(3,9),(4,3),(4,2),(4,5),(5,1),(5,2),(5,10),(6,1),(6,6),(6,20),(7,2),(7,8),(7,17),(8,2),(8,9),(8,10);

insert into Actor (first_name, last_name) values
  ('Emma','Eatson'),('Dan','Stevens'),('Luke','Evans'),
  ('Hugh','Jackman'),('Patrick','Stewart'),('Dafne','Keen'),
  ('Matthew','McConaughey'),('Reese','Titherspoon'),('Seth','MacFarlane'),
  ('Auli\'i','Cravalho'),('Dwayne','Johnson'),('Rachel','House'),
  ('Tom','Hiddleston'),('Samuel','L. Jackson'),('Brie','Larson'),
  ('Charlize','Theron'),('Vin','Diesel'),
  ('Jennifer','Lawrence'),('Chris','Pratt'),('Michael','Sheen'),
  ('Eddie','Redmayne'),('Katherine','Waterston'),('Alison','Sudol');

insert into Movie_Actor (movie_id, actor_id, role_name) values
  (1,1,'Belle'),(1,2,'Beast'),(1,3,'Gaston'),
  (2,4,'Logan'),(2,5,'Charles'),(2,6,'Laura'),
  (3,7,'Booster Moon'),(3,8,'Rosita'),(3,9,'Mike'),
  (4,10,'Moana'),(4,11,'Maui'),(4,12,'Gramma Tala'),
  (5,13,'James Conrad'),(5,14,'Prston Packard'),(5,15,'Mason Weaver'),
  (6,16,'Cipher'),(6,11,'Hobbs'),(6,17,'Dominic Toretto'),
  (7,18,'Aurora Lane'),(7,19,'Jim Preston'),(7,20,'Arthur'),
  (8,21,'Newt'),(8,22,'Tina'),(8,23,'Queenie');

insert into Director (first_name, last_name) values
  ('Bill','Condon'),('James','Mangold'),('Garth','Jennings'),('Christophe Laurdelet',''),('Ron','Clements'),('Don','Hall'),('John','Musker'),('Chris','Williams'),('Jordan','Vogt-Roberts'),('F. Gary','Gray'),('Morten','Tyldum'),('David','Yates');

insert into Movie_Director (movie_id, director_id, description) values
  (1,1,'directed by'),(2,2,'directed by'),(3,3,'directed by'),(3,4,'co-director'),(4,5,'directed by'),(4,6,'co-director'),(4,7,'directed by'),(4,8,'co-director'),(5,9,'directed by'),(6,10,''),(7,11,'directed by'),(8,12,'directed by');

insert into Writer (first_name, last_name) values
  ('Stephen','Chbosky'),('Evan','Spiliotopoulos'),('James','Mangold'),('Scott','Frank'),('Michael','Green'),('Garth','Jennings'),('Jared','Bush'),('Dan','Gilroy'),('Max','Borenstein'),('Derek','Connolloy'),('John','Gatins'),('Chris','Morgan'),('Gary Scott','Thompson'),('Jon','Spaihts'),('J.K.','Rowling');

insert into Movie_Writer (movie_id, writer_id, description) values
  (1,1,'screenplay'),(1,2,'screenplay'),(2,3,'story by'),(2,4,'screenplay'),(2,3,'screenplay'),(2,5,'screenplay'),(3,6,'written by'),(4,7,'screenplay'),(5,8,'screenplay'),(5,9,'screenplay'),(5,10,'screenplay'),(5,11,'story by'),(6,12,'written by'),(6,13,'based on characters created by'),(7,14,'written by'),(8,15,'written by');

