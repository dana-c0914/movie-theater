create table Authority (
  id bigint primary key AUTO_INCREMENT,
  name varchar(50));

create table Users (
  id bigint primary key AUTO_INCREMENT,
  first_name varchar(50),
  family_name varchar(50),
  e_mail varchar(50),
  phone varchar(50),
  language char(2),
  id_picture varchar(20),
  login varchar(50) NOT NULL UNIQUE,
  password varchar(50),
  burth_date Date,
  enabled boolean);

create table Users_Authority (
  id bigint primary key AUTO_INCREMENT,
  id_user BIGINT,
  id_authority BIGINT,
  foreign key (id_user) REFERENCES Users(id),
  foreign key (id_authority) REFERENCES Authority(id)
);

create table Token (
  series varchar(50) primary key,
  value varchar(50),
  date timestamp,
  ip_address varchar(50),
  user_agent varchar(200),
  user_login varchar(50));

create table Movie (
  id bigint primary key auto_increment,
  title varchar(255),
  description varchar(2500),
  release_year int,
  length_minutes int
);

create table Category (
  id bigint primary key auto_increment,
  title varchar(255)
);

create table Movie_Category (
#   id bigint primary key auto_increment,
  movie_id bigint,
  category_id bigint,
  primary key (movie_id,category_id),
  foreign key (movie_id) REFERENCES Movie(id),
  foreign key (category_id) REFERENCES Category(id)
);

create table Actor (
  id bigint primary key auto_increment,
  first_name varchar(50),
  last_name varchar(50),
  actor_id_picture varchar(20)
);

create table Movie_Actor (
  movie_id bigint,
  actor_id bigint,
  role_name varchar(255),
  primary key (movie_id,actor_id),
  foreign key (movie_id) REFERENCES Movie(id),
  foreign key (actor_id) REFERENCES Actor(id)
);

create table Director (
  id bigint primary key auto_increment,
  first_name varchar(50),
  last_name varchar(50),
  director_id_picture varchar(20)
);

create table Movie_Director (
  movie_id bigint,
  director_id bigint,
  description varchar(255),
  primary key (movie_id,director_id),
  foreign key (movie_id) REFERENCES Movie(id),
  foreign key (director_id) REFERENCES Director(id)
);

create table Writer (
  id bigint primary key auto_increment,
  first_name varchar(50),
  last_name varchar(50),
  writer_id_picture varchar(20)
);

create table Movie_Writer (
  movie_id bigint,
  writer_id bigint,
  description varchar(255),
  primary key (movie_id,writer_id),
  foreign key (movie_id) REFERENCES Movie(id),
  foreign key (writer_id) REFERENCES Writer(id)
);