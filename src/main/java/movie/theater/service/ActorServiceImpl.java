package movie.theater.service;

import movie.theater.dto.ActorDto;
import movie.theater.persist.entity.Actor;
import org.springframework.stereotype.Service;

/**
 * Created by danac on 21.03.2017.
 */
@Service
public class ActorServiceImpl extends GenericServiceImpl<Actor, ActorDto, Long> {
}
