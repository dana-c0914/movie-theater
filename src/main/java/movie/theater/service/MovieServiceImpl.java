package movie.theater.service;

import movie.theater.dto.MovieDto;
import movie.theater.persist.entity.Movie;
import org.springframework.stereotype.Service;

/**
 * Created by danac on 21.03.2017.
 */
@Service
public class MovieServiceImpl extends GenericServiceImpl<Movie, MovieDto, Long> {
}
