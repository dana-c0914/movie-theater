package movie.theater.service;

import movie.theater.dto.WriterDto;
import movie.theater.persist.entity.Writer;
import org.springframework.stereotype.Service;

/**
 * Created by danac on 21.03.2017.
 */
@Service
public class WriterServiceImpl extends GenericServiceImpl<Writer, WriterDto, Long> {
}
