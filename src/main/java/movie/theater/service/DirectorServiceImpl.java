package movie.theater.service;

import movie.theater.dto.DirectorDto;
import movie.theater.persist.entity.Director;
import org.springframework.stereotype.Service;

/**
 * Created by danac on 21.03.2017.
 */
@Service
public class DirectorServiceImpl extends GenericServiceImpl<Director, DirectorDto, Long> {
}
