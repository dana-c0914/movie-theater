package movie.theater.service;

import movie.theater.dto.CategoryDto;
import movie.theater.persist.entity.Category;
import org.springframework.stereotype.Service;

/**
 * Created by danac on 21.03.2017.
 */
@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category, CategoryDto, Long> {
}
