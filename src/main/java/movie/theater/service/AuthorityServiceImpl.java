package movie.theater.service;

import movie.theater.dto.AuthorityDto;
import movie.theater.persist.entity.Authority;
import org.springframework.stereotype.Service;

@Service
public class AuthorityServiceImpl extends GenericServiceImpl<Authority, AuthorityDto, Long>{
}
