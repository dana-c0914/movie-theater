//package movie.theater.dto.converters;
//
//import movie.theater.persist.entity.Writer;
//import org.dozer.DozerConverter;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Created by danac on 22.03.2017.
// */
//public class WriterToIdConverter extends DozerConverter<List<Writer>, List<Long>> {
//
//    public WriterToIdConverter(Class<List<Writer>> prototypeA, Class<List<Long>> prototypeB) {
//        super(prototypeA, prototypeB);
//    }
//
//    @Override
//    public List<Long> convertTo(List<Writer> writers, List<Long> longs) {
//        return writers.stream().map(Writer::getId).collect(Collectors.toList());
//    }
//
//    @Override
//    public List<Writer> convertFrom(List<Long> longs, List<Writer> writers) {
//        return null;
//    }
//}
