//package movie.theater.dto.converters;
//
//import movie.theater.persist.entity.Director;
//import org.dozer.DozerConverter;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Created by danac on 22.03.2017.
// */
//public class DirectorToIdConverter extends DozerConverter<List<Director>, List<Long>> {
//
//    public DirectorToIdConverter(Class<List<Director>> prototypeA, Class<List<Long>> prototypeB) {
//        super(prototypeA, prototypeB);
//    }
//
//    @Override
//    public List<Long> convertTo(List<Director> directors, List<Long> longs) {
//        return directors.stream().map(Director::getId).collect(Collectors.toList());
//    }
//
//    @Override
//    public List<Director> convertFrom(List<Long> longs, List<Director> directors) {
//        return null;
//    }
//}
