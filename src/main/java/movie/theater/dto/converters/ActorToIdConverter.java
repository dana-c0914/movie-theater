//package movie.theater.dto.converters;
//
//import movie.theater.persist.entity.Actor;
//import org.dozer.DozerConverter;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Created by danac on 22.03.2017.
// */
//public class ActorToIdConverter extends DozerConverter<List<Actor>, List<Long>> {
//
//    public ActorToIdConverter(Class<List<Actor>> prototypeA, Class<List<Long>> prototypeB) {
//        super(prototypeA, prototypeB);
//    }
//
//    @Override
//    public List<Long> convertTo(List<Actor> actors, List<Long> longs) {
//        return actors.stream().map(Actor::getId).collect(Collectors.toList());
//    }
//
//    @Override
//    public List<Actor> convertFrom(List<Long> longs, List<Actor> actors) {
//        return null;
//    }
//}
