package movie.theater.dto;

import movie.theater.persist.entity.MovieActor;
import org.dozer.Mapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 21.03.2017.
 */
public class ActorDto {
    @Mapping("id")
    private Long id;

    @Mapping("firstName")
    private String firstName;

    @Mapping("lastName")
    private String lastName;

    @Mapping("actorIdPicture")
    private int actorIdPicture;

    @Mapping("movieActors")
    private Set<MovieActor> movieActors = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getActorIdPicture() {
        return actorIdPicture;
    }

    public void setActorIdPicture(int actorIdPicture) {
        this.actorIdPicture = actorIdPicture;
    }

    public Set<MovieActor> getMovieActors() {
        return movieActors;
    }

    public void setMovieActors(Set<MovieActor> movieActors) {
        this.movieActors = movieActors;
    }
}
