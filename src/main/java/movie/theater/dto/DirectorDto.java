package movie.theater.dto;

import movie.theater.persist.entity.MovieDirector;
import org.dozer.Mapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 21.03.2017.
 */
public class DirectorDto {
    @Mapping("id")
    private Long id;

    @Mapping("firstName")
    private String firstName;

    @Mapping("lastName")
    private String lastName;

    @Mapping("directorIdPicture")
    private int directorIdPicture;

    @Mapping("movieDirectors")
    private Set<MovieDirector> movieDirectors = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDirectorIdPicture() {
        return directorIdPicture;
    }

    public void setDirectorIdPicture(int directorIdPicture) {
        this.directorIdPicture = directorIdPicture;
    }

    public Set<MovieDirector> getMovieDirectors() {
        return movieDirectors;
    }

    public void setMovieDirectors(Set<MovieDirector> movieDirectors) {
        this.movieDirectors = movieDirectors;
    }
}
