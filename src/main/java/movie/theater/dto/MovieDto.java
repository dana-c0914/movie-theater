package movie.theater.dto;

import org.dozer.Mapping;

/**
 * Created by danac on 21.03.2017.
 */
public class MovieDto {
    @Mapping("id")
    private Long id;

    @Mapping("title")
    private String title;

    @Mapping("description")
    private String description;

    @Mapping("releaseYear")
    private int releaseYear;

    @Mapping("lengthMinutes")
    private int lengthMinutes;

    @Mapping("posterId")
    private String posterId;

//    private Set<Long> categories = new HashSet<>();
//
//    private Set<Long> movieActors = new HashSet<>();
//
//    private Set<Long> movieDirectors = new HashSet<>();
//
//    private Set<Long> movieWriters = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getLengthMinutes() {
        return lengthMinutes;
    }

    public void setLengthMinutes(int lengthMinutes) {
        this.lengthMinutes = lengthMinutes;
    }

    public String getPosterId() {
        return posterId;
    }

    public void setPosterId(String posterId) {
        this.posterId = posterId;
    }

    //    public Set<Long> getCategories() {
//        return categories;
//    }
//
//    public void setCategories(Set<Long> categories) {
//        this.categories = categories;
//    }
//
//    public Set<Long> getMovieActors() {
//        return movieActors;
//    }
//
//    public void setMovieActors(Set<Long> movieActors) {
//        this.movieActors = movieActors;
//    }
//
//    public Set<Long> getMovieDirectors() {
//        return movieDirectors;
//    }
//
//    public void setMovieDirectors(Set<Long> movieDirectors) {
//        this.movieDirectors = movieDirectors;
//    }
//
//    public Set<Long> getMovieWriters() {
//        return movieWriters;
//    }
//
//    public void setMovieWriters(Set<Long> movieWriters) {
//        this.movieWriters = movieWriters;
//    }
}
