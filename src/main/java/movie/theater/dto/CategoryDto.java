package movie.theater.dto;

import movie.theater.persist.entity.Movie;
import org.dozer.Mapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 21.03.2017.
 */
public class CategoryDto {
    @Mapping("id")
    private Long id;

    @Mapping("title")
    private String title;

    @Mapping("movies")
    private Set<Movie> movies = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
