package movie.theater.dto;

import movie.theater.persist.entity.MovieWriter;
import org.dozer.Mapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 21.03.2017.
 */
public class WriterDto {
    @Mapping("id")
    private Long id;

    @Mapping("firstName")
    private String firstName;

    @Mapping("lastName")
    private String lastName;

    @Mapping("writerIdPicture")
    private int writerIdPicture;

    @Mapping("movieWriters")
    private Set<MovieWriter> movieWriters = new HashSet<>();
}
