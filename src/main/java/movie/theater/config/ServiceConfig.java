package movie.theater.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("movie.theater.service")
public class ServiceConfig {
   
     

}
