package movie.theater.persist.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 20.03.2017.
 */
@Entity
@Table(name = "Movie")
public class Movie {
    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "release_year", nullable = false)
    private int releaseYear;

    @Column(name = "length_minutes", nullable = false)
    private int lengthMinutes;

    @Column(name = "poster_id", nullable = false)
    private String posterId;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Movie_Category", joinColumns = { @JoinColumn(name = "movie_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "category_id", table = "Category", referencedColumnName = "id") })
    private Set<Category> categories = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.movie")
    private Set<MovieActor> movieActors = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.movie")
    private Set<MovieDirector> movieDirectors = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.movie")
    private Set<MovieWriter> movieWriters = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getLengthMinutes() {
        return lengthMinutes;
    }

    public void setLengthMinutes(int lengthMinutes) {
        this.lengthMinutes = lengthMinutes;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<MovieActor> getMovieActors() {
        return movieActors;
    }

    public void setMovieActors(Set<MovieActor> movieActors) {
        this.movieActors = movieActors;
    }

    public Set<MovieDirector> getMovieDirectors() {
        return movieDirectors;
    }

    public void setMovieDirectors(Set<MovieDirector> movieDirectors) {
        this.movieDirectors = movieDirectors;
    }

    public Set<MovieWriter> getMovieWriters() {
        return movieWriters;
    }

    public void setMovieWriters(Set<MovieWriter> movieWriters) {
        this.movieWriters = movieWriters;
    }
}
