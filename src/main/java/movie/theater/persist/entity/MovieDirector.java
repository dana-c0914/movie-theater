package movie.theater.persist.entity;

import movie.theater.persist.entity.primaryKeys.MovieDirectorId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Entity
@Table(name = "Movie_Director")
@AssociationOverrides({
        @AssociationOverride(name = "pk.movie",
                joinColumns = @JoinColumn(name = "movie_id")),
        @AssociationOverride(name = "pk.director",
                joinColumns = @JoinColumn(name = "director_id")) })
public class MovieDirector implements Serializable {
    @EmbeddedId
    private MovieDirectorId pk = new MovieDirectorId();
    @Column(name = "description")
    private String description;

    public MovieDirectorId getPk() {
        return pk;
    }

    public void setPk(MovieDirectorId pk) {
        this.pk = pk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public Movie getMovie() {
        return getPk().getMovie();
    }


    public void setMovie(Movie movie) {
        getPk().setMovie(movie);
    }

    @Transient
    public Director getdirector() {
        return getPk().getDirector();
    }


    public void setDirector(Director director) {
        getPk().setDirector(director);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieDirector that = (MovieDirector) o;

        return pk != null ? pk.equals(that.pk) : that.pk == null;
    }

    @Override
    public int hashCode() {
        return pk != null ? pk.hashCode() : 0;
    }
}
