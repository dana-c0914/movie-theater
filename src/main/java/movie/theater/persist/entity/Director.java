package movie.theater.persist.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danac on 20.03.2017.
 */
@Entity
@Table(name = "Director")
public class Director {
    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "director_id_picture")
    private String directorIdPicture;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.director")
    private Set<MovieDirector> movieDirectors = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDirectorIdPicture() {
        return directorIdPicture;
    }

    public void setDirectorIdPicture(String directorIdPicture) {
        this.directorIdPicture = directorIdPicture;
    }

    public Set<MovieDirector> getMovieDirectors() {
        return movieDirectors;
    }

    public void setMovieDirectors(Set<MovieDirector> movieDirectors) {
        this.movieDirectors = movieDirectors;
    }
}
