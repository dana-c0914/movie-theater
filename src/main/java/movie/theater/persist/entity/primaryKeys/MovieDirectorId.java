package movie.theater.persist.entity.primaryKeys;

import movie.theater.persist.entity.Director;
import movie.theater.persist.entity.Movie;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Embeddable
public class MovieDirectorId implements Serializable {

    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Director director;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieDirectorId that = (MovieDirectorId) o;

        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        return director != null ? director.equals(that.director) : that.director == null;
    }

    @Override
    public int hashCode() {
        int result = movie != null ? movie.hashCode() : 0;
        result = 31 * result + (director != null ? director.hashCode() : 0);
        return result;
    }
}
