package movie.theater.persist.entity.primaryKeys;

import movie.theater.persist.entity.Actor;
import movie.theater.persist.entity.Movie;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Embeddable
public class MovieActorId implements Serializable {
    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Actor actor;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieActorId that = (MovieActorId) o;

        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        return actor != null ? actor.equals(that.actor) : that.actor == null;
    }

    @Override
    public int hashCode() {
        int result = movie != null ? movie.hashCode() : 0;
        result = 31 * result + (actor != null ? actor.hashCode() : 0);
        return result;
    }
}
