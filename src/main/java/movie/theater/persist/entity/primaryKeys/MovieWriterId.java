package movie.theater.persist.entity.primaryKeys;

import movie.theater.persist.entity.Movie;
import movie.theater.persist.entity.Writer;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Embeddable
public class MovieWriterId implements Serializable {
    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Writer writer;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Writer getWriter() {
        return writer;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieWriterId that = (MovieWriterId) o;

        if (movie != null ? !movie.equals(that.movie) : that.movie != null) return false;
        return writer != null ? writer.equals(that.writer) : that.writer == null;
    }

    @Override
    public int hashCode() {
        int result = movie != null ? movie.hashCode() : 0;
        result = 31 * result + (writer != null ? writer.hashCode() : 0);
        return result;
    }
}
