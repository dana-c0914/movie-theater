package movie.theater.persist.entity;

import movie.theater.persist.entity.primaryKeys.MovieWriterId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Entity
@Table(name = "Movie_Writer")
@AssociationOverrides({
        @AssociationOverride(name = "pk.movie",
                joinColumns = @JoinColumn(name = "movie_id")),
        @AssociationOverride(name = "pk.writer",
                joinColumns = @JoinColumn(name = "writer_id")) })
public class MovieWriter implements Serializable {
    @EmbeddedId
    private MovieWriterId pk = new MovieWriterId();
    @Column(name = "description")
    private String description;

    public MovieWriterId getPk() {
        return pk;
    }

    public void setPk(MovieWriterId pk) {
        this.pk = pk;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public Movie getMovie() {
        return getPk().getMovie();
    }


    public void setMovie(Movie movie) {
        getPk().setMovie(movie);
    }

    @Transient
    public Writer getWriter() {
        return getPk().getWriter();
    }


    public void setWriter(Writer writer) {
        getPk().setWriter(writer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieWriter that = (MovieWriter) o;

        return pk != null ? pk.equals(that.pk) : that.pk == null;
    }

    @Override
    public int hashCode() {
        return pk != null ? pk.hashCode() : 0;
    }
}
