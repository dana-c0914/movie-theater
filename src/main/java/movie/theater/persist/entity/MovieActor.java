package movie.theater.persist.entity;

import movie.theater.persist.entity.primaryKeys.MovieActorId;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by danac on 20.03.2017.
 */
@Entity
@Table(name = "Movie_Actor")
@AssociationOverrides({
        @AssociationOverride(name = "pk.movie",
                joinColumns = @JoinColumn(name = "movie_id")),
        @AssociationOverride(name = "pk.actor",
                joinColumns = @JoinColumn(name = "actor_id")) })
public class MovieActor implements Serializable {

    @EmbeddedId
    private MovieActorId pk = new MovieActorId();
    @Column(name = "role_name")
    private String roleName;

    public MovieActorId getPk() {
        return pk;
    }

    public void setPk(MovieActorId pk) {
        this.pk = pk;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Transient
    public Movie getMovie() {
        return getPk().getMovie();
    }


    public void setMovie(Movie movie) {
        getPk().setMovie(movie);
    }

    @Transient
    public Actor getActor() {
        return getPk().getActor();
    }


    public void setActor(Actor actor) {
        getPk().setActor(actor);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MovieActor that = (MovieActor) o;

        return pk != null ? pk.equals(that.pk) : that.pk == null;
    }

    @Override
    public int hashCode() {
        return pk != null ? pk.hashCode() : 0;
    }
}
