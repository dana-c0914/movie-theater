package movie.theater.persist;

import movie.theater.persist.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by danac on 21.03.2017.
 */
public interface MovieRepo extends JpaRepository<Movie, Long> {
}
