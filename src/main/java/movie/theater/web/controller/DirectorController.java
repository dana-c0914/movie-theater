package movie.theater.web.controller;

import movie.theater.dto.DirectorDto;
import movie.theater.service.DirectorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by danac on 21.03.2017.
 */
@RestController
@RequestMapping("/directors")
public class DirectorController {
    @Autowired
    private DirectorServiceImpl directorService;

    @RequestMapping(method = RequestMethod.GET)
    public List<DirectorDto> getAllDirectors() {
        return directorService.findAll();
    }

    @RequestMapping(value = "/{directorId}", method = RequestMethod.GET)
    public DirectorDto getDirectorById(@PathVariable Long directorId) {
        return directorService.findOne(directorId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public DirectorDto saveDirector(@RequestBody DirectorDto director) {
        directorService.save(director);
        return director;
    }
}
