package movie.theater.web.controller;

import movie.theater.dto.ActorDto;
import movie.theater.service.ActorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by danac on 21.03.2017.
 */
@RestController
@RequestMapping("/actors")
public class ActorController {
    @Autowired
    private ActorServiceImpl actorService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ActorDto> getAllActors() {
        return actorService.findAll();
    }

    @RequestMapping(value = "/{actorId}", method = RequestMethod.GET)
    public ActorDto getActorById(@PathVariable Long actorId) {
        return actorService.findOne(actorId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ActorDto saveActor(@RequestBody ActorDto actor) {
        actorService.save(actor);
        return actor;
    }
}
