package movie.theater.web.controller;

import movie.theater.dto.WriterDto;
import movie.theater.service.WriterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by danac on 21.03.2017.
 */
@RestController
@RequestMapping("/writers")
public class WriterController {
    @Autowired
    private WriterServiceImpl writerService;

    @RequestMapping(method = RequestMethod.GET)
    public List<WriterDto> getAllWriters() {
        return writerService.findAll();
    }

    @RequestMapping(value = "/{writerId}", method = RequestMethod.GET)
    public WriterDto getWriterById(@PathVariable Long writerId) {
        return writerService.findOne(writerId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public WriterDto saveWriter(@RequestBody WriterDto writer) {
        writerService.save(writer);
        return writer;
    }
}
