package movie.theater.web.controller;

import movie.theater.dto.MovieDto;
import movie.theater.service.MovieServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by danac on 21.03.2017.
 */
@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    private MovieServiceImpl movieService;

    @RequestMapping(method = RequestMethod.GET)
    public List<MovieDto> getAllMovies() {
        return movieService.findAll();
    }

    @RequestMapping(value = "/{movieId}", method = RequestMethod.GET)
    public MovieDto getMovieById(@PathVariable Long movieId) {
        return movieService.findOne(movieId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public MovieDto saveMovie(@RequestBody MovieDto movie) {
        movieService.save(movie);
        return movie;
    }
}
