package movie.theater.web.controller;

import movie.theater.dto.CategoryDto;
import movie.theater.service.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by danac on 21.03.2017.
 */
@RestController
@RequestMapping("/categories")
public class CategoryController {
    @Autowired
    private CategoryServiceImpl categoryService;

    @RequestMapping(method = RequestMethod.GET)
    public List<CategoryDto> getAllCategories() {
        return categoryService.findAll();
    }

    @RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
    public CategoryDto getCategoryById(@PathVariable Long categoryId) {
        return categoryService.findOne(categoryId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public CategoryDto saveCategory(@RequestBody CategoryDto category) {
        categoryService.save(category);
        return category;
    }
}
